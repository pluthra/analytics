# Creating a New User in Snowflake

- [ ] Create user using SECURITYADMIN role
- [ ] Create user specific role
- [ ] Assign user specific role
- [ ] Assign user in Okta
- [ ] Verify grants
- [ ] Securely send user their temporary credentials 
- [ ] Update `roles.yml`
